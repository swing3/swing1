/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener: Action");
    }
}

/**
 *
 * @author Satit Wapeetao
 */
public class Hello implements ActionListener{

    public static void main(String[] args) {
        JFrame femMain = new JFrame("Hello bro");
        femMain.setSize(500, 300);
        femMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourName = new JLabel("Your Name");
        lblYourName.setSize(75, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.green);
        lblYourName.setOpaque(true);
        JTextField txtxYourName = new JTextField();
        txtxYourName.setSize(200, 20);
        txtxYourName.setLocation(90, 5);

        JButton btnHello = new JButton("Hellp");
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);
        
        MyActionListener myAction = new MyActionListener();
        btnHello.addActionListener(myAction);
        btnHello.addActionListener(new Hello());
        
        ActionListener actionListener= new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class:Action");
            }
             
        };
        btnHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello",JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        femMain.setLayout(null);

        femMain.add(lblYourName);
        femMain.add(txtxYourName);
        femMain.add(btnHello);
        femMain.add(lblHello);
        
        btnHello.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name= txtxYourName.getText();
                lblHello.setText("Hello"+name);
            }
        
        });
        femMain.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Hello bro :Action");
    }
}
