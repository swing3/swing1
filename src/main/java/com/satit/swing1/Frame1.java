/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.swing1;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Satit Wapeetao
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(500, 300));

        JLabel lblHelloWorld = new JLabel("Hello World!!!", JLabel.CENTER);
        lblHelloWorld.setBackground(Color.GREEN);
        lblHelloWorld.setOpaque(true);
        frame.add(lblHelloWorld);

        frame.setVisible(true);

    }
}
